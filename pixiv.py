try:
    from pixivpy3 import *
    import urllib
    import os
    import sys
    import credentials
    import pprint
    import re
    import json
except ImportError:
    pass

USER_ID = '11738946'
REFRESH_TOKEN = credentials.REFRESH_TOKEN

aapi = AppPixivAPI()
aapi.auth(refresh_token=REFRESH_TOKEN)

json_result = aapi.user_bookmarks_illust(USER_ID)

directory = "dl"
if not os.path.exists(directory):
    os.makedirs(directory)


def differentOriginal(illust):
    if illust.meta_single_page.get('original_image_url', illust.image_urls.large):
        return illust.meta_single_page.get('original_image_url', illust.image_urls.large)
    else:
        return illust['meta_pages'][0]['image_urls']['original']


def couldNotDownload(title, url, illust_id):
    print("Could not download: " + "%s: %s" %
          (title, url))
    output_file = open("output.txt", "a", encoding="utf-8")
    output_file.write("title: " + title +
                      "---url: https://www.pixiv.net/member_illust.php?mode=medium&illust_id=" + str(illust_id) + "\n")
    output_file.close()


def nextPage(nextUrl):
    try:
        output_file = open("next.txt", "a", encoding="utf-8")
        output_file.write(nextUrl+"\n")
        output_file.close()
    except:
        pass


def loopAllImages(json_result, selectPage):
    for illust in json_result.illusts:
        try:
            if len(illust.meta_pages):
                for idx, picture in enumerate(illust.meta_pages):
                    image_url = picture.get(
                        'original_image_url', picture.image_urls.original)
                    url_basename = os.path.basename(image_url)
                    extension = os.path.splitext(url_basename)[1]
                    name = "illust_id_%d_%s%s%s" % (
                        illust.id, illust.title, idx, extension)
                    print("%s: %s" % (name, image_url))

                    aapi.download(image_url, path=directory, name=name)
            else:
                image_url = differentOriginal(illust)

                print("%s: %s" % (illust.title, image_url))

                url_basename = os.path.basename(image_url)
                extension = os.path.splitext(url_basename)[1]
                name = "illust_id_%d_%s%s" % (
                    illust.id, illust.title, extension)
                aapi.download(image_url, path=directory, name=name)
        except:
            couldNotDownload(illust.title, image_url, illust.id)
            continue

    if selectPage == True:
        next_qs = aapi.parse_qs(
            'https://app-api.pixiv.net/v1/user/bookmarks/illust?user_id=11738946&restrict=public&filter=for_ios&max_bookmark_id=4003878079')
        json_results = aapi.user_bookmarks_illust(**next_qs)
        selectPage = False
    else:
        # Uncomment to log pages
        # nextPage(json_result.next_url)
        next_qs = aapi.parse_qs(json_result.next_url)
        nextPage(json_result.next_url)
        json_results = aapi.user_bookmarks_illust(**next_qs)
    loopAllImages(json_results, selectPage)


loopAllImages(json_result, selectPage=False)
